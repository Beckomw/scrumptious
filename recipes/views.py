from django.shortcuts import render, redirect, get_object_or_404
from recipes.models import Recipe 
from recipes.forms import RecipeForm  

def get_background_color():
    if 'bg-color' not in request.session:
        request.session['bg-color'] = 'bg-color-1'
    else: 
        current_

def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)

    context = {
        "recipe_object": recipe,
    }

    return render(request, "recipes/detail.html", context)



def recipe_list(request):
    recipes = Recipe.objects.all()

    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)




def create_recipe(request):
    if request.method == "POST":
        form = RecipeForm(request.POST)

        if form.is_valid():
            form.save()
            return redirect("recipe_list")
    
    else:
        form = RecipeForm()

        context = {
            "form": form,
        }
    return render(request, "recipes/create.html", context)



def default(request):
    return redirect('recipe_list')




def edit_recipe(request, id):
    post = get_object_or_404(Recipe, id=id)
    if request.method == "POST": 
        form = RecipeForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect(show_recipe, id=post.id)
    else: 
        form = RecipeForm(instance=post)

    context = {
        "form": form,
        "post": post,
    }

    return render(request, "recipes/edit.html", context)