from django.urls import path
from recipes.views import recipe_list, show_recipe,create_recipe,edit_recipe
from recipes import views



urlpatterns = [
    path('', views.recipe_list, name='recipe_list'),
    path('recipe/<int:id>/', views.show_recipe, name='show_recipe'),
    path('recipes/create/', views.create_recipe, name='create_recipe'),
    path('recipes/edit/<int:id>/', views.edit_recipe, name='edit_recipe')
]